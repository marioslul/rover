/*
 * rover.c
 *
 * Created: 04.04.2020 17:10:21
 * Author : Marios
 */ 
#define F_CPU 8000000 
#include <avr/io.h>
#include <avr/interrupt.h>
#include "servo/servo.h"
#include "motors/motors.h"
#include <util/delay.h>
#include "utils/utils.h"
#include "uart/rs485.h"
#include <string.h>

void intenable(void) //enable interrupts (globally)
{
	SREG |= (1 << 7);
}

void intdisable(void) //disable interrupts (globally)
{
	SREG &= ~(1 << 7);
}

void init_loop(void) 
{
	uint8_t i;
	
	for (i = 0; i < 5; i++)
	{
		led_clear();
		_delay_ms(100);
		led_set();
		_delay_ms(250);
	}
}

int main(void)
{
	//int i = 0;
	servo_init();
	motors_init();
	rs485_init();
	
	OCR1B = 0;
	OCR1A = 0;

	motors_enter_standby();
	intenable();
		
	init_loop();

    /* Replace with your application code */
    while (1) 
    {
		rs485_receive_service();

		
		//if (OCR1A < 255)
		//{
			//OCR1B += 1;
			//OCR1A += 1;
		//}

		//OCR3A = 2500;
		//OCR3B = 2500;
		/*for (i = 500; i < 2500; i+=1)
		{
			OCR3A = i;
			OCR3B = i;	
			_delay_ms(3);
		}
				for (i = 2500; i > 500; i-=1)
				{
					OCR3A = i;
					OCR3B = i;
					_delay_ms(3);
				}*/
    }
}

