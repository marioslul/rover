/*
 * main.h
 *
 * Created: 04.04.2020 20:25:48
 *  Author: Marios
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#include <avr/io.h>

//PIN_BATTERY		=	PF0 (ADC0) 4,7k, 10k, 10k
//PIN_PHOTORESISTOR =	PF1 (ADC1)
//PIN_ADC_CONN1 =		PF2 (ADC2) (upper side of board)
//PIN_ADC_CONN2 =		PF3 (ADC3)
//PIN_ADC_CONN3 =		PF4 (ADC4)
//PIN_ADC_CONN4 =		PF5 (ADC5)
//PIN_ADC_CONN5 =       PF6 (ADC6)
//PIN_ADC_CONN6 =		PF7 (ADC7)
//PIN_LIGHTS_2  =		PB7 (OC2/OC1C)
//PIN_SERVO_1   =		PE3 (OC3A)
//PIN_SERVO_2   =       PE4 (OC3B)
//PIN_MOTOR1_PWN =		PB5 (OC1A)
//PIN_MOTOR2_PWM =		PB6 (OC1B)

#define PIN_BATTERY (1 << 0)

#define PORT_SERVO		PORTE
#define DIR_SERVO		DDRE
#define PIN_SERVO_1		(1 << 3)
#define PIN_SERVO_2		(1 << 4)

#define PORT_MOTOR_PWM PORTB
#define DIR_MOTOR_PWM DDRB

#define PIN_MOTOR_LEFT_PWM (1 << 5)
#define PIN_MOTOR_RIGHT_PWM (1 << 6)

#define PORT_MOTOR_RIGHT_BIN12 PORTC
#define DIR_MOTOR_RIGHT_BIN12 DDRC
#define PIN_MOTOR_RIGHT_BIN2 (1 << 3) //PC3
#define PIN_MOTOR_RIGHT_BIN1 (1 << 2) //PC2

#define PORT_MOTOR_LEFT_AIN12 PORTC
#define DIR_MOTOR_LEFT_AIN12 DDRC
#define PIN_MOTOR_LEFT_AIN2 (1 << 0) //PC0
#define PIN_MOTOR_LEFT_AIN1 (1 << 1) //PC1

#define PORT_MOTORS_STANDBY PORTC
#define DIR_MOTORS_STANDBY DDRC
#define PIN_MOTORS_STANDBY (1 << 4) //PC4

void intenable(void);
void intdisable(void);

#endif /* MAIN_H_ */