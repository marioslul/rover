/*
 * spi_drv.h
 *
 * Created: 10.04.2020 20:34:55
 *  Author: Marios
 */ 


#ifndef SPI_DRV_H_
#define SPI_DRV_H_


void spi_init(void);
void spi_transmit_receive(uint8_t* out_buf, uint8_t* in_buf, uint8_t len);
void spi_transmit(uint8_t* out_buf, uint8_t len);
uint8_t spi_transmit_receive_byte(uint8_t data);


#endif /* SPI_DRV_H_ */