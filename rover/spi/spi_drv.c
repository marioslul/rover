/*
 * spi_drv.c
 *
 * Created: 10.04.2020 20:34:41
 *  Author: Marios
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#define PORT_SPI    PORTB
#define DIR_SPI     DDRB
#define PIN_MISO     (1 << 3)
#define PIN_MOSI     (1 << 2)
#define PIN_SS       (1 << 0)
#define PIN_SCK      (1 << 1)


void spi_init(void)
{
	DIR_SPI &= ~(PIN_MOSI|PIN_MISO|PIN_SS|PIN_SCK);
	// Define the following pins as output
	DIR_SPI |= (PIN_MOSI|PIN_SS|PIN_SCK);

	
	SPCR = ((1<<SPE)|               // SPI Enable
	(0<<SPIE)|              // SPI Interupt Enable
	(0<<DORD)|              // Data Order (0:MSB first / 1:LSB first)
	(1<<MSTR)|              // Master/Slave select
	(0<<SPR1)|(1<<SPR0)|    // SPI Clock Rate
	(0<<CPOL)|              // Clock Polarity (0:SCK low / 1:SCK hi when idle)
	(0<<CPHA));             // Clock Phase (0:leading / 1:trailing edge sampling)

	SPSR = (1<<SPI2X);              // Double Clock Rate
	
}

void spi_transmit_receive(uint8_t* out_buf, uint8_t* in_buf, uint8_t len)
{
	uint8_t i;
	for (i = 0; i < len; i++) 
	{
		SPDR = out_buf[i];
		while((SPSR & (1<<SPIF)) == 0);
		in_buf[i] = SPDR;
	}
}

void spi_transmit(uint8_t* out_buf, uint8_t len)
{
	uint8_t i;
	for (i = 0; i < len; i++) 
	{
		SPDR = out_buf[i];
		while((SPSR & (1<<SPIF)) == 0);
	}
}

uint8_t spi_transmit_receive_byte(uint8_t data)
{
	SPDR = data;
	while((SPSR & (1<<SPIF)) == 0);
	return SPDR;
}