/*
 * motors.c
 *
 * Created: 04.04.2020 23:17:11
 *  Author: Marios
 */ 

#include "../main.h"
#include "motors.h"

static void motor_right_cw(void);
static void motor_right_ccw(void);
static void motor_left_cw(void);
static void motor_left_ccw(void);

void motors_init(void)
{
	//DIR_SERVO |= (PIN_SERVO_1|PIN_SERVO_2); //set as outputs
	DIR_MOTOR_PWM |= (PIN_MOTOR_LEFT_PWM|PIN_MOTOR_RIGHT_PWM); //set as outputs
	DIR_MOTOR_LEFT_AIN12 |= (PIN_MOTOR_LEFT_AIN1|PIN_MOTOR_LEFT_AIN2); //set as outputs
	DIR_MOTOR_RIGHT_BIN12 |= (PIN_MOTOR_RIGHT_BIN1|PIN_MOTOR_RIGHT_BIN2); //set as outputs
	DIR_MOTORS_STANDBY |= (PIN_MOTORS_STANDBY); //set as output
		
	TCCR1A |= (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);
	TCCR1B |= (1<<WGM13)|(1<<WGM12)|(1<<CS10); //fast mode, prescaler = 8
	ICR1 = 255; //~31,4 kHz
		
	PORT_MOTORS_STANDBY |= PIN_MOTORS_STANDBY; 
		
	OCR1A = 0;
	OCR1B = 0;
	
	motor_right_ccw();
	motor_left_ccw();
}

void motors_enter_standby(void)
{
	PORT_MOTORS_STANDBY &= ~PIN_MOTORS_STANDBY;
}

void motors_release_standby(void)
{
	PORT_MOTORS_STANDBY |= PIN_MOTORS_STANDBY;
}

void motors_set_pwm(uint8_t left, uint8_t right)
{
	OCR1A = left;
	OCR1B = right;
}

static void motor_right_cw(void)
{
	PORT_MOTOR_RIGHT_BIN12 |= PIN_MOTOR_RIGHT_BIN1;
	PORT_MOTOR_RIGHT_BIN12 &= ~(PIN_MOTOR_RIGHT_BIN2);
}

static void motor_right_ccw(void)
{
	PORT_MOTOR_RIGHT_BIN12 &= ~(PIN_MOTOR_RIGHT_BIN1);
	PORT_MOTOR_RIGHT_BIN12 |= PIN_MOTOR_RIGHT_BIN2;
}

static void motor_left_cw(void)
{
	PORT_MOTOR_LEFT_AIN12 |= PIN_MOTOR_LEFT_AIN1;
	PORT_MOTOR_LEFT_AIN12 &= ~(PIN_MOTOR_LEFT_AIN2);
}

static void motor_left_ccw(void)
{
	PORT_MOTOR_LEFT_AIN12 &= ~(PIN_MOTOR_LEFT_AIN1);
	PORT_MOTOR_LEFT_AIN12 |= PIN_MOTOR_LEFT_AIN2;
}

void motors_forward(void)
{
	motor_left_cw();
	motor_right_cw();
}

void motors_backward(void)
{
	motor_left_ccw();
	motor_right_ccw();
}

void motors_rotate_left(void)
{
	motor_left_cw();
	motor_right_ccw();
}

void motors_rotate_right(void)
{
	motor_left_ccw(); //PWM levels will be responsible for turning
	motor_right_cw();
}

void motors_turn_left(void)
{
	motor_left_cw(); //PWM levels will be responsible for turning
	motor_right_cw();
}
void motors_turn_right(void)
{
	motor_left_cw(); //PWM levels will be responsible for turning
	motor_right_cw();
}