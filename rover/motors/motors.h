/*
 * motors.h
 *
 * Created: 04.04.2020 23:17:24
 *  Author: Marios
 */ 


#ifndef MOTORS_H_
#define MOTORS_H_

#include <stddef.h>

void motors_init(void);

void motors_enter_standby(void);
void motors_release_standby(void);


void motors_set_pwm(uint8_t left, uint8_t right);
void motors_forward(void);
void motors_backward(void);
void motors_rotate_left(void);
void motors_rotate_right(void);
void motors_turn_left(void);
void motors_turn_right(void);



#endif /* MOTORS_H_ */