/*
 * adc.h
 *
 * Created: 05.04.2020 01:34:28
 *  Author: Marios
 */ 


#ifndef ADC_H_
#define ADC_H_

typedef enum
{
	ADC_BATTERY_CHANNEL = 0x00,
	ADC_PHOTO_CHANNEL,
	ADC_EXT1_CHANNEL,
	ADC_EXT2_CHANNEL,
	ADC_EXT3_CHANNEL,
	ADC_EXT4_CHANNEL,
	ADC_EXT5_CHANNEL,
	ADC_EXT6_CHANNEL
}adc_channel_t;

void adc_init(void);
void adc_select_channel(adc_channel_t channel);


#endif /* ADC_H_ */