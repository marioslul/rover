/*
 * rs485.h
 *
 * Created: 10.04.2020 22:46:05
 *  Author: Marios
 */ 


#ifndef RS485_H_
#define RS485_H_

#include <stdint.h>
#include "../utils/utils.h"

void rs485_init(void);
aec_t rs485_send_data(uint8_t* data, uint16_t length);
aec_t rs485_get_byte(uint8_t* out_byte);
void rs485_receive_service(void);

#endif /* RS485_H_ */