/*
 * rs485.c
 *
 * Created: 10.04.2020 22:46:20
 *  Author: Marios
 */ 

#include "../utils/circular_buffer.h"
#include "rs485.h"
#include "avr/interrupt.h"
#include "../control/control.h"
#include <string.h>

#define BUFSIZE 256
uint8_t rx_buffer[BUFSIZE];
uint8_t tx_buffer[BUFSIZE];

cbuf_handle_t rx_circ_buf;
cbuf_handle_t tx_circ_buf;

#define DIR_RS485 DDRD
#define PORT_RS485 PORTD
#define PIN_RS485_FLOW (1 << 4)
#define PIN_RS485_TX   (1 << 3)
#define PIN_RS485_RX   (1 << 2)

static inline void rs485_switch_tx(void);
static inline void rs485_switch_rx(void);

uint8_t frame_buffer[sizeof(command_frame_t)];

void rs485_init(void)
{
	rx_circ_buf = circular_buf_init(rx_buffer, sizeof(rx_buffer));
	tx_circ_buf = circular_buf_init(tx_buffer, sizeof(tx_buffer));
	
	DIR_RS485 |= PIN_RS485_TX;
	DIR_RS485 |= PIN_RS485_FLOW;
	
	UBRR1L = 51;  //fosc = 8M, target baudrate = 250k (0% error rate), U2X = 0
	UBRR1H = 0;
	
	UCSR1B |=	(1 << RXCIE1)| //RX complete interrupt enable
				(1 << TXCIE1)| //TX complete interrupt enable 
				(1 << RXEN1)|  //receiver enable
				(1 << TXEN1);  //transmitter enable
	UCSR1C |=	(1 << UCSZ11)|
				(1 << UCSZ10); //8 bit mode
	
	
	rs485_switch_rx();
}

static inline void rs485_switch_tx(void)
{
	PORT_RS485 |= PIN_RS485_FLOW;
}

static inline void rs485_switch_rx(void)
{
	PORT_RS485 &= ~PIN_RS485_FLOW;
}

aec_t rs485_send_data(uint8_t* data, uint16_t length)
{
	uint16_t i;
	bool invoke_transmit = 0;
	
	invoke_transmit = circular_buf_empty(tx_circ_buf);
	
	for (i = 0; i < length; i++)
	{
		if (circular_buf_put2(tx_circ_buf, data[i]) != 0)
		{
			critical_error();
			return AEC_CIRCULAR_BUFFER_FULL;
		}
	}
	
	if (invoke_transmit)
	{
		circular_buf_get(tx_circ_buf, (uint8_t*) &i); //reuse i intentionally
		rs485_switch_tx();
		UDR1 = i;
	}
	
	return AEC_OK;
}

void rs485_receive_service(void)
{
	static uint8_t received_bytes = 0;
	uint8_t byte = 0;
	static command_frame_t current_command;
	
	if (circular_buf_get(rx_circ_buf, &byte) == 0)
	{
		frame_buffer[received_bytes] = byte;
		received_bytes++;
		if (received_bytes == sizeof(command_frame_t))
		{
			received_bytes = 0;
			memcpy((void*)&current_command, frame_buffer, sizeof(command_frame_t));
			parse_rs485_command(&current_command);
			
			circular_buf_reset(rx_circ_buf);
			//led_clear();
		}
	}
	
		
}

aec_t rs485_get_byte(uint8_t* out_byte)
{
	if (circular_buf_get(rx_circ_buf, out_byte) != 0) return AEC_CIRCULAR_BUFFER_EMPTY;
	
	return AEC_OK;
}

ISR(USART1_RX_vect)
{
	//led_set();
	circular_buf_put2(rx_circ_buf, (uint8_t)UDR1);
}

ISR(USART1_TX_vect)
{
	uint8_t tx_byte;
	if (circular_buf_get(tx_circ_buf, &tx_byte) == 0)
	{
		rs485_switch_tx();
		UDR1 = tx_byte;
	}
	else 
	{
		rs485_switch_rx();
	}
}






