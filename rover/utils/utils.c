/*
 * utils.c
 *
 * Created: 05.04.2020 01:22:13
 *  Author: Marios
 */ 
#define F_CPU 8000000
#include <stdint.h>
#include "utils.h"
#include <util/delay.h>

static uint32_t tick = 0;

static void inline inc_tick(void)
{
	tick += 1;
}

uint32_t get_tick(void)
{
	return tick;
}

void led_set(void)
{
	TEMP_LED_DIR &= ~(TEMP_LED_PIN);
	TEMP_LED_PORT &= ~(TEMP_LED_PIN);
}

void led_clear(void)
{
	TEMP_LED_DIR |= TEMP_LED_PIN;
	TEMP_LED_PORT &= ~(TEMP_LED_PIN);
}

void led_indicate_critical_error(void)
{
	while (1)
	{
		led_clear();
		_delay_ms(40);
		led_set();
		_delay_ms(40);
	}
}

void critical_error(void) //placeholder for critical error service routine
{
	led_indicate_critical_error(); //temporary
}

void assert_critical(int condition)
{
	if (condition) critical_error();
}