/*
 * utils.h
 *
 * Created: 05.04.2020 01:22:01
 *  Author: Marios
 */ 


#ifndef UTILS_H_
#define UTILS_H_

#include <avr/io.h>
#include "error_codes.h"

#define TEMP_LED_DIR DDRE
#define TEMP_LED_PORT PORTE
#define TEMP_LED_PIN (1 << 7)

void led_init(void);
void led_set(void);
void led_clear(void);
void led_indicate_critical_error(void);
void critical_error(void);
void assert_critical(int condition);


#endif /* UTILS_H_ */