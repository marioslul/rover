/*
 * error_codes.h
 *
 * Created: 11.04.2020 01:13:46
 *  Author: Marios
 */ 


#ifndef ERROR_CODES_H_
#define ERROR_CODES_H_

typedef enum
{
	AEC_OK								= 0x0000,
	AEC_CIRCULAR_BUFFER_FULL			= 0x0100,
	AEC_CIRCULAR_BUFFER_EMPTY			= 0x0101,
	
}aec_t; //action exit code



#endif /* ERROR_CODES_H_ */