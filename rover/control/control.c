/*
 * control.c
 *
 * Created: 11.04.2020 15:40:27
 *  Author: Marios
 */ 
#define F_CPU 80000000
#include "control.h"
#include "../motors/motors.h"
#include "avr/io.h"
#include "../utils/utils.h"
#include <util/delay.h>
#include <string.h>

static command_frame_t previous_command;

void parse_rs485_command(command_frame_t* cmd)
{
	uint8_t i;
	
	if (cmd == NULL) critical_error();
	if (cmd->ctrl_byte != 0x69)
	{
		for (i = 0; i < 3; i++)
		{
			led_clear();
			_delay_ms(400);
			led_set();
			_delay_ms(400);
		}
	}
	
	if (previous_command.cmd == cmd->cmd) //if the command is the same as previous one, update only PWM levels
	{
		motors_set_pwm(cmd->left_motor_pwm, cmd->right_motor_pwm);
		return;
	}
	
	switch(cmd->cmd)
	{
		case CMD_STANDBY: motors_enter_standby(); break;
		case CMD_FORWARD:
		case CMD_BACKWARD:
		case CMD_ROTATE_LEFT:
	    case CMD_ROTATE_RIGHT:
		case CMD_TURN_LEFT:
		case CMD_TURN_RIGHT:
		{
			/* place for general actions before every direction change */
			motors_enter_standby();
			motors_set_pwm(cmd->left_motor_pwm, cmd->right_motor_pwm);
			switch(cmd->cmd) //lower level analisys
			{
				case CMD_FORWARD: motors_forward(); break;
				case CMD_BACKWARD: motors_backward(); break;
				case CMD_ROTATE_LEFT: motors_rotate_left(); break;
				case CMD_ROTATE_RIGHT: motors_rotate_right(); break;
				case CMD_TURN_LEFT: motors_turn_left(); break;
				case CMD_TURN_RIGHT: motors_turn_right(); break;
				default: break;
			}
			motors_release_standby();
		}
		default: break;
	}
	
	memcpy((void*)&previous_command, (void*)cmd, sizeof(command_frame_t));
}