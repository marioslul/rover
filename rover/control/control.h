/*
 * control.h
 *
 * Created: 11.04.2020 15:40:45
 *  Author: Marios
 */ 


#ifndef CONTROL_H_
#define CONTROL_H_

#include <stdint.h>

typedef enum
{
	CMD_STANDBY =		0x00,
	CMD_FORWARD =		0x10,
	CMD_BACKWARD =		0x11,
	CMD_ROTATE_LEFT =	0x12,
	CMD_ROTATE_RIGHT =	0x13,
	CMD_TURN_LEFT =		0x14,
	CMD_TURN_RIGHT =	0x15,
	
	
	CMD_MAX =			0xFF
}command_t;

typedef struct  
{
	uint8_t ctrl_byte;
	command_t cmd;
	uint8_t left_motor_pwm;
	uint8_t right_motor_pwm;
}command_frame_t;


void parse_rs485_command(command_frame_t* cmd);



#endif /* CONTROL_H_ */