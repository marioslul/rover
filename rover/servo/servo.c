/*
 * servo.c
 *
 * Created: 04.04.2020 20:51:58
 *  Author: Marios
 */ 

#define F_CPU 8000000
#include "../main.h"
#include "servo.h"
#include <avr/io.h>

void servo_init(void)
{
	DIR_SERVO |= (PIN_SERVO_1|PIN_SERVO_2); //set as outputs
	
	TCCR3A |= (1<<COM3A1)|(1<<COM3B1)|(1<<WGM31);
	TCCR3B |= (1<<WGM33)|(1<<WGM32)|(1<<CS31); //fast mode, prescaler = 8
	ICR3 = 20000; //for 50 Hz,
	
	OCR3A = 1500;
	OCR3B = 1500;
}